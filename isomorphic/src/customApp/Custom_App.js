import React from 'react';
import {IntlProvider} from 'react-intl'
import Custom_App_Holder from './containers/Custom_App_Holder'

import AppLocale from '../languageProvider';
import config, {
  getCurrentLanguage,
} from '../containers/LanguageSwitcher/config';


const currentAppLocale =
  AppLocale[getCurrentLanguage(config.defaultLanguage || 'english').locale];

const Custom_App = () =>(
    //passing the local to the intl provider and using it to wrap the intier application
    <IntlProvider
        locale={currentAppLocale.locale}
        messages={currentAppLocale.messages}>

        <Custom_App_Holder/>

    </IntlProvider>
)

export default Custom_App 