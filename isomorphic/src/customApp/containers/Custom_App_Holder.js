import React from 'react';
import Form from '../components/Form'
import {FormattedMessage} from 'react-intl'
import UserDetails from '../components/UserDetails';

const Custom_App_Holder = () => (

    <div className="container-fluid">
        <div className="row">
            <div className="col-2" >

                <div className="row justify-content-center">
                    <h3>
                        <FormattedMessage id="custom_app.sidebar.brand"/>
                    </h3>
                </div>

                <div className="row bg-dark" style={{paddingBottom: "2000px"}}>
                    
                    <div className="container">
                        <ul className="p-4 text-light">

                            <li  style={{padding: "12px", fontSize: "18px"}}>
                                <a href="" className="text-decoration-none">
                                    <FormattedMessage id="custom_app.sidebar.menu"/>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
            <div className="col-8">
                 <div className="row"><Form/></div>
                 <div className="row"><UserDetails/></div>
            </div>
        </div>

    </div>
    


)


export default Custom_App_Holder