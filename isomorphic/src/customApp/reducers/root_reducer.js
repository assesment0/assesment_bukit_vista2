const init_state = {
   
    //user data to be displayed after being retrieved from the server. Initialiy empty
    name: "",
    property_name: "",
    check_in_date: "",
    check_out_date: "",
    arrival_time : "", 

    //when this is true that means we have the user's data from an api query returned
    //from an api call using the booking code, and when we aquire the data we set this to true
    //indicating to UserDetails component that it can be displayed with the user data 
    //retrieved by the query
    valid_code_entered: false 
}

const root_reducer = (state = init_state, action) => {
   
    //activates when a user enter a code for the first time
    if(action.type === 'CODE_ENTERED'){

        return {
            ...state,
            name: action.user_data.name,
            property_name: action.user_data.property_name,
            check_in_date: action.user_data.check_in_date,
            check_out_date: action.user_data.check_out_date,
            arrival_time: action.user_data.arrival_time,
            valid_code_entered: true,
        }
    }

    if(action.type === 'ARRIVAL_TIME_CHANGED'){

        //send the new arrival time to the server
        //but I don't have an api so i can't do it
        //but we can update the state with the new arrival time
        
        //console.log("new_arrival_time: " + (new Date(action.new_arrival_time).toISOString()))
        return {
            ...state,
            arrival_time: action.new_arrival_time
        }
    }

    //default action
    return state;
}

export default root_reducer;
