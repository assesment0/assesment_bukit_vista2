import React,{Component} from 'react'
import {FormattedMessage} from 'react-intl'
import {connect} from 'react-redux'
import store from '../../index'
import {TimePicker} from 'antd'

class UserDetails extends Component {

    render () {

        return (
            <div 
            id="UserDetails" 
            className="container p-5" 
            style= {{visibility: (this.props.component_visibilty) ? "visible" : "hidden"}}
            >

                <div className="row">

                    <div className="col-3">
                        <img 
                        src="" 
                        class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" 
                        alt="User Image"/>
                    </div>
                </div>

                <div className="row p-3">
                    <h5 className="h5"><FormattedMessage id="custom_app.user.hi"/> {this.props.name}!</h5>
                </div>

                <div className="row p-3">
                    <p><FormattedMessage id="custom_app.user.thanks"/></p>
                </div>

                <div className="row p-3">
                    <div className="col-4"><FormattedMessage id="custom_app.user.property"/>:  </div>
                    <div className="col-8"><strong>{this.props.property_name}</strong></div>
                </div>

                
                <div className="row p-3">
                    <div className="col-3"><FormattedMessage id="custom_app.user.check_in_date"/>:   </div>
                    <div className="col-3">
                        <strong>
                            {(this.props.check_in_date !== "") ? (new Date()).toDateString(this.props.check_in_date) : ""}
                        </strong>
                    </div>

                    <div className="col-3"><FormattedMessage id="custom_app.user.check_out_date"/>:   </div>
                    <div className="col-3">
                        <strong>
                            {(this.props.check_out_date !== "") ? (new Date()).toDateString(this.props.check_out_date) : ""}
                        </strong>
                    </div>
                </div>

                <div className="row p-3">
                    <div className="col-2"><FormattedMessage id="custom_app.user.Arrival_date"/>:</div>
                    <div className="col-5">
                        <TimePicker 
                        value={this.props.arrival_time}
                        onChange={ (value) => store.dispatch({type: 'ARRIVAL_TIME_CHANGED', new_arrival_time: value})}
                        />
                    </div>
                    <div className="col-5">
                        <strong className={this.props.arrival_time === "" ? "text-danger" : "text-success"}>
                        <FormattedMessage 
                        id={this.props.arrival_time === "" ? "custom_app.user.Arrival_date_alternative" : "custom_app.user.Arrival_date_known"}
                        />
                        </strong>
                    </div>

                </div>


            </div>
        )

    }
}

//update the fields of the data everytime they are changed
const mapStateToProps = (state) => {

    return {

        name: state.name,
        property_name: state.property_name,
        check_in_date: state.check_in_date,
        check_out_date: state.check_out_date,
        arrival_time: state.arrival_time,
        component_visibilty: state.valid_code_entered
    }

}

export default connect(mapStateToProps)(UserDetails)