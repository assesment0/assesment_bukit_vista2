import React, {Component} from 'react'
import {FormattedMessage} from 'react-intl'
import store from '../../index'
 

class Form extends Component{
  
    state = {
        warning_text: "",
        booking_code: "",   
    }

    code_chagned = (e) => {

        e.preventDefault();
        this.setState({
            ...this.state,
            booking_code: e.target.value,
        });
        this.state.booking_code = e.target.value;
        this.update_warning_text();
    }
    
    update_warning_text() {

        //matching the code entered by the user to make sure it is valid
        //as it only contains capital letters and numbers with no spaces or symbols
        const code_regex = /[A-Z0-9]*/g
        let code = this.state.booking_code;
        let warning = (code.match(code_regex)[0] === code && code.match(code_regex)[0].length === code.length)
        ? ("")
        :(<FormattedMessage id="custom_app.booking_code_warning"/>);

        this.setState({
            ...this.state,
            warning_text: warning,
        })
        
    }


    handle_submit = (e) =>{

        e.preventDefault();
        const code_regex = /[A-Z0-9]*/g
        let code = this.state.booking_code;

        if( ! (code.match(code_regex)[0] === code &&
         code.match(code_regex)[0].length === code.length) 
         || code === "") // if the booking code is invalid or empty do not submit or initiate the query
        {return;}


        
        //we here initiate a dummy api response
        //passing a simple object as an api response object
        const new_data = {
            name: "Ammar",
            property_name: "Villa",
            check_in_date: "2010-3-15",
            check_out_date: "2010-3-20",
            arrival_time: "",
            valid_code_entered: true,
        }

        store.dispatch({type: 'CODE_ENTERED', user_data: new_data});

    }

    render(){


        return (
            <div className="container pt-5" onSubmit={this.handle_submit}>
                
                <form>
                    <div class="form-group">
                            <label for="booking_code"><FormattedMessage id="custom_app.prompt"/> </label>
                            
                            <input type="input" class="form-control" id="booking_code" placeholder="KJSH87HGDK" onChange={this.code_chagned}/>
                            <small id="booking_code_warnging_text" class="form-text text-danger">{this.state.warning_text}</small>
                    </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        )
    }
}


export default Form