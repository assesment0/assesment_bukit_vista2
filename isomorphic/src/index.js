import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import 'antd/dist/antd.css';
import Custom_App from './customApp/Custom_App' 
import {createStore} from 'redux'
import { Provider } from 'react-redux'
import root_reducer from './customApp/reducers/root_reducer';

const store = createStore(root_reducer);

ReactDOM.render(<Provider store={store}> <Custom_App /></Provider>, document.getElementById('root'));
serviceWorker.register();

export default store;